## ssh公钥配置

#### 1. 打开控制台


```
git bash
```

#### 2. 输入以下命令后


```
ssh-keygen -t rsa
```


#### 3. Overwrite (y/n)?


```
y => enter => enter
```


#### 4. 找到并打开 id_rsa.pub 文件



```
cd                  // 返回根目录
cd .ssh             // 进入 .ssh 文件
ls                  // 查看当前目录 
cat id_rsa.pub      // 打开 id_rsa.pub
```



#### 5. 复制公钥配置到 github 账户上



```
 github个人中心 => setting => SSH and GPG keys => SSH keys => 添加公钥
```


#### 6. 克隆带ssh选项的地址, 再进行克隆


视频教程: [如何在macos上配置ssh并免密拉取git仓库内容](https://www.bilibili.com/video/BV1uv411Y7ci?from=search&seid=11678540056676885735&spm_id_from=333.337.0.0)

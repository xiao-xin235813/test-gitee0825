# git常见错误处理

### 01.fetch first 错误



>  _! [rejected] master -> master (fetch first) error: failed to push some refs to ' 。。。'_


原因: 出现这个问题是因为github中的README.md文件不在本地代码目录中，可以通过如下命令进行代码合并

```
git pull --rebase origin master
```


### 02.不小心将node_modules传到远端

```js

/* 在.gitignore中配置node_modules */
/node_modules

/* 将node_modules从远端删除 */
git rm -r --cached node_modules

/* 重新提交git */
git add .
git commit -m 'xxx'
git push 
```


### 将远端拉取后代码代码修改后，再链接到新的远程分支，添加暂存区时候，出现Untracked files：/files
```
reason: 在files的子目录中还存在.git的隐藏文件，所以报错
resolve：删除子目录.git即可
```


### git warning
>  _! warning: There are too many unreachable loose objects; run 'git prune' to remove them._

```
git gc  --prune=now 
```

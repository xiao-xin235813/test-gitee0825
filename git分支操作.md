# git分支操作


### 删除demo分支
```
git checkout master // 保证当前分支不是demo分支
git branch -D demo  // 删本地demo
git push origin --delete demo // 删远程demo
```


### 创建demo分支推送demo
```
git checkout -b demo
git push -u origin demo
```


### 在dev分支拉demo分支
```
git fetch origin demo
```


### 基于远程分支 origin/demo 进行创建demo分支
```
// 本地没有demo分支，基于远程分支 origin/demo 进行创建demo分支
git checkout -b demo origin/demo
```


### 查看分支
```
git brnach // 查看本地
git branch -a // 查看本地和远程
```


### 查看本地和远程关联
```
git branch -vv // 查看关联
// * dev    f1d3d6220 [origin/dev]  // *(当前分支) 本地分支 commit记录 [远程分支]
//   master f1d3d6220 [origin/master] 
//   xinyi  f1d3d6220 
```


### 将远端默认分支设置为master
```
git branch -a // 查看分支，若没有远程分支关联master
//  dev
//* master
//  xinyi
//  remotes/origin/dev
//  remotes/origin/master
//  remotes/origin/wys

git remote set-head origin master // 将远程默认分支关联到master

git branch -a  // 查看origin/HEAD指向origin/master，关联成功
//  dev
//* master
//  xinyi
//  remotes/origin/HEAD -> origin/master
//  remotes/origin/dev
//  remotes/origin/master
//  remotes/origin/wys
```

# git版本控制的基本操作(单分支)

### 创建本地仓库

创建.gitignore文本, 并配置好

```
git init
git add .
git commit -m "init app"
```

### 创建远程仓库

```
New Repo
指定仓库名
创建
```


### 将本地和远端连接

```
git remote add origin url (在本地记录远程仓库的地址)
git push -u origin master
```

### 修改代码, 提交本地, 推送远端

```
git add -A
git commit -m "xxx"
git push
```

### 记住用户和密码

```
git config --global credential.helper store
```

### 远端代码有修改, 先拉后推

```
git pull
git push
```

### 克隆远端代码到本地

```
git clone url
```

# git版本控制的基本操作(多分支)

### 创建本地分支,推送远端

```
git checkout -b 分支名
git push -u origin 分支名
```

### 本地分支开发, 并推送到远程

```
git add .
git commit -m "xxx"
git push
```

### 根据远程个人开发分支创建本地个人开发分支

```
git pull  (如果分支是在clone后创建的才需要执行)
git checkout -b 分支名 origin/分支名
```

### 将个人开发分支合并到master

```
git checkout master
git merge atguigu
git push
```
